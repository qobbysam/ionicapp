angular.module('starter.controllers', ['ngCordova', 'ngFileUpload','ngMessages'])

  .controller('MasterCtrl', function ($scope, $state, djangoAuth) {

  })

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state) {
  $scope.send_search = function(){
    $state.go('app.search');
  };

  $scope.send_create = function(){
    $state.go("app.create");
  };

})



.controller('CategoryhomeCtrl', function($scope, CategoryPageService, $stateParams, $http){
  $scope.do_load = false;
  $scope.data = [];

  CategoryPageService.get($stateParams.name).then(function(data){
   // console.log(data);
    angular.forEach(data.data.results, function(child){
     // console.log(child);
      $scope.data.push(child);
    });
    //$scope.data.push(data.data.results);
    $scope.next_url = data.data.next;

    if ($scope.next_url !== null){
      $scope.do_load = true;
    }
  
   var checkEmpty = function (obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};

   $scope.check = checkEmpty(data.data.results);

  }).catch(function(err){
  //console.log(err);
});

function check_do_load(){
  if ($scope.next_url !== null){
    $scope.do_load = true;
  }
  else {
    $scope.do_load = false;
  }
}

  $scope.loadMoreData = function(){
    //alert("doing load more");
    if ($scope.next_url!== null){
      $http.get($scope.next_url).then(function(res){
      //  alert("success of get");
      angular.forEach(res.data.results, function(child){
     // console.log(child);
      $scope.data.push(child);
    });
        $scope.next_url = res.data.next;
        $scope.$broadcast('scroll.infiniteScrollComplete');
        check_do_load();

      }). catch(function(err){
      //  console.log(err);
      //  alert("error of get");
      });
    }
    else { $scope.do_load = false;}
  };

  })
  
  

.controller('AdCtrl', function($scope, AdService, $stateParams,$ionicSlideBoxDelegate){
  var adpk = $stateParams.adId;
  //console.log(adpk);

  /* Initialises the  App with all the data Needed to Display the ad
  Do_get cleans out the received images and returns a list for showing the Images in a slide box
  */
        var do_get = function(obj){
              var lists = [];

            for(var key1 in obj) {
              var im = obj[key1];
                if (Object.getOwnPropertyNames(im).length!==0 ){
                  if (im.image.main !== undefined){
                    lists.push(im.image.main);
                  }
                  
                }
            }

            if (lists.length === 0) {
              lists.push("http://kadiel.com/media/__sized__/__placeholder__/no_image400-thumbnail-400x400.jpg");
            }

       // console.log(lists);
        return lists;

  };





    AdService.get(adpk).then(function(data){


      $scope.adData = data.data;
    
      var uncleanimgs = do_get(data.data.imgs);
    
      $scope.images = uncleanimgs;
     // console.log($scope.images);

    setTimeout(function() {
    $ionicSlideBoxDelegate.slide(0);
    $ionicSlideBoxDelegate.update();
    $scope.$apply();
} );

  });






})

.controller('homeCtrl', function($scope,$stateParams,$state){

})

.controller('CreatePostCtrl', function($scope,$stateParams,$state, $auth, $ionicHistory,CheckAuthStatus, CreatePostService){
    var user_details = JSON.parse(localStorage.getItem('user'));
 // console.log (user_details);
   $ionicHistory.nextViewOptions({
    disableBack: true
  });
   $scope.content={};
   $scope.cat_selected = true;
   $scope.opt_selected =true;
   $scope.content.category = 1;
   $scope.content.delivery="p";
  $scope.prepare = function(data){

    post_data = {
      ad:{
        title: data.title,
        city: data.location,
        category: data.category,
        ad_fields:{
          price:data.price,
          delivery_options:data.delivery,
          phone_number: data.phonenumber,
          more_detail: data.moredetail,
        },
      }, 
      owner: JSON.stringify(user_details.id),
      allow_world: true,
    };

    CreatePostService.send(post_data).then(function(res){
   //   console.log(res);
      n_id = res.data.id;
    //  console.log(n_id);

      $state.go('app.addimgs',{adId:n_id});
    }).catch(function(err){
    //  console.log(err);

    });


};



})

.controller('LoginCtrl', function($scope, $state, $rootScope, $ionicPopup, $http,$auth, $window, $ionicHistory, $stateParams){
 // console.log('Iam in login');
  //console.log($scope.user);
  var next_to_url = $stateParams.next;
  $scope.show_error = false;
  //$scope.error = "username and password did not match"
 $ionicHistory.nextViewOptions({
    disableBack: true
  });
 // var vm = this;

  //var loc = window.location.origin ;
  //alert(loc);

  //vm.loginError = false;
  //vm.loginErrorText;

  $scope.send_signup = function(){
    $state.go("app.signup");
  };
  
  $scope.do_login = function(user) {

      var dat = {
      username: user.username, 
      password: user.password
      };
    //  console.log(dat);

     $auth.login(dat).then(function(response) {

    //  console.log(response);
      $auth.setToken(response);

      $rootScope.authenticated = true;

      // var token = $auth.getToken();
      // console.log(token);

      $http({
      url: "http://kadiel.com/rest-auth/user/",
      method: "GET",

       })
      .then(function(res){

                  var user = JSON.stringify(res.data);

                  //console.log(user.username);

                  $window.localStorage.setItem('user', user);
                  // $window.localStorage.setItem('email', res.data.username);
                  // $window.localStorage.setItem('id', res.data.username);
                        $rootScope.currentUser = res.data.username;


       })

       .catch(function(res){
       // console.log(res);
        

       });
      
      $state.go(next_to_url);
  })
  .catch(function(error) {
    //  console.log(error);
      $scope.show_error = true;

  });
  };

  //Social login 
      $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function(res) {
      $http({
      url: "http://kadiel.com/rest-auth/user/",
      method: "GET",

       })
      .then(function(res){

                  var user = JSON.stringify(res.data);

                  //console.log(user.username);

                  $window.localStorage.setItem('user', user);
                  // $window.localStorage.setItem('email', res.data.username);
                  // $window.localStorage.setItem('id', res.data.username);
                  $rootScope.currentUser = res.data.username;
                  $state.go(next_to_url);


       })

       .catch(function(res){
       // console.log(res);
       });



        })
        .catch(function(response) {
          $ionicPopup.alert({
            title: 'Error',
            content: response.data ? response.data || response.data.message : response
          });
       //   console.log(response);

        });
    };
  
})



.controller('LogoutCtrl' ,function($scope, $state, $auth, $ionicHistory){

 $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $scope.do_logout = function(){
    $auth.logout();
    localStorage.removeItem('user');
    $state.go('app.home');
  };
  $scope.cancel_logout = function(){
    $state.go('app.home');
  };
})

.controller('SignupCtrl' ,function($scope, $state, $http, $window, $rootScope,$auth, $ionicHistory){
   $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $scope.email_error =false;
  $scope.username_error = false;
  $scope.password_error = false;
 var do_login_op = function(data){
    var send_data = {
    username:data.username,
    password:data.password
    };
 
    $auth.login(send_data).then(function(response){

    //  console.log(response);
      $auth.setToken(response);
      $rootScope.authenticated = true;

      $http.get('http://kadiel.com/rest-auth/user/')
      
      .then(function(res){

          var user = JSON.stringify(res.data);

          $window.localStorage.setItem('user', user);
          $rootScope.currentUser = res.data.username;


       })

       .catch(function(err){
      //  console.log(err);
       });

      
      $state.go('app.profile');

    }).catch(function(err){
      console.error(err);
      $scope.show_error = true;
    });
  };
  $scope.do_signup = function(info){
    var send_data = { username: info.username,
                  password1:info.password1,
                  password2:info.password2,
                  email : info.email
    };

    $auth.signup(send_data).then(function(res){
    //  console.log("signup success");
      info = {username:send_data.username,password:send_data.password1};
      do_login_op(info);
      }).catch(function(err){
   //     console.log(err);
        $scope.errors = err.data;
        $scope.show_error = true;
      });
      };

})

.controller('SettingsCtrl', function($scope, $state, $auth, $rootScope){
  var user_details = localStorage.getItem('user');
 // console.log (user_details);

  $scope.local_auth_status = $auth.isAuthenticated();

  $scope.user_info = user_details;



  $scope.signout = function(){
    $state.go('app.logout');
  };

})

.controller('EditAdCtrl' ,function($scope, $state, $stateParams){
  var user_details = localStorage.getItem('user');
 // console.log (user_details);
  $scope.ad_id = $stateParams.adId;

  $scope.user_info = user_details;

  $scope.do_edit_image = function(){
    $state.go('app.addimgs', {adId:$scope.ad_id});
  };
  $scope.do_edit_ad = function(){
    $state.go('app.visit_site');
  };

})

.controller('AdImgsCtrl' ,function($scope, $state, $ionicNavBarDelegate, $stateParams,$ionicActionSheet,Upload,$ionicLoading,CreatePostService,$cordovaCamera,$timeout, $cordovaImagePicker){
  
  $scope.$on('$ionicView.enter', function(e) {
    $ionicNavBarDelegate.showBar(true);
});


  var user_details = localStorage.getItem('user');
  $scope.show_page = false;
  $scope.user_info = user_details;

  var ad_id = $stateParams.adId;

//Determine to show change or select file

  function prepare_img_scope(pk,loc){
    
    CreatePostService.img_get(pk).then(function(res){
      if (loc == 1){
        $scope.img_1_src = res.data.image;
        if($scope.img_1_src === null || $scope.img_1_src === ""){
          $scope.show_img_1 = true;
        }
        else{
          $scope.show_img_1 = false;
        }
      }
      if (loc == 2){
        $scope.img_2_src = res.data.image;
        if($scope.img_2_src === null|| $scope.img_2_src === ""){
          $scope.show_img_2 = true;
        }
        else{
          $scope.show_img_2 = false;
        }
      }
      if (loc == 3){
        $scope.img_3_src = res.data.image;
        if($scope.img_3_src === null || $scope.img_3_src === ""){
          $scope.show_img_3 = true;
        }
        else{
          $scope.show_img_3 = false;
        }
      }
      if (loc == 4){
        $scope.img_4_src = res.data.image;
        if($scope.img_4_src === null|| $scope.img_4_src === ""){
          $scope.show_img_4 = true;
        }
        else{
          $scope.show_img_4 = false;
        }
      }
      if (loc == 5){
        $scope.img_5_src = res.data.image;
        if($scope.img_5_src === null || $scope.img_5_src === ""){
          $scope.show_img_5 = true;
        }
        else{
          $scope.show_img_5 = false;
        }
      }
    })
    .catch(function(err){
      $scope.show_img_1 = true;
      $scope.show_img_2 = true;
      $scope.show_img_3 = true;
      $scope.show_img_4 = true;
      $scope.show_img_5 = true;

    });

  }

  // Do this first on page load;

  $scope.init_op = function (){
    CreatePostService.prep(ad_id).then(function(res){
    //  console.log(res.data);
      $scope.al_list_id = res.data.al_list;
      $scope.img_1_pk = res.data.img_1;
      $scope.img_2_pk = res.data.img_2;
      $scope.img_3_pk = res.data.img_3;
      $scope.img_4_pk = res.data.img_4;
      $scope.img_5_pk = res.data.img_5;

      prepare_img_scope($scope.img_1_pk, 1);
      prepare_img_scope($scope.img_2_pk, 2);
      prepare_img_scope($scope.img_3_pk, 3);
      prepare_img_scope($scope.img_4_pk, 4);
      prepare_img_scope($scope.img_5_pk, 5);

      $scope.show_page = true;

      



    }).catch(function(err){
    //  console.log(err);


    });



  };



  function sendImg(data,al,pk){
    var alpk = al;
    var imgpk = pk;

    var name_of_file = makeid();

    var img_data = blobToFile(data, name_of_file);

    var dataimg =  img_data ;



    //alert('doing sendIMg' + alpk + " " + imgpk + "  " + dataimg + name_of_file );

    CreatePostService.img_send(file=dataimg,al=alpk,pk=imgpk).then(function(res){

    //  console.log("success file uploaded");
     // alert('send success ' + res.data.id + res.data);
      //$state.go($state.current, $stateParams, {reload: true, inherit: false});
      $state.reload();



    }).catch(function(err){
      //alert("send_fail");
   //   console.log("error did not upload");
    //  console.error(err);
      //alert("send_fail" + err.data);
      //$state.go($state.current, $stateParams, {reload: true, inherit: false});
      $scope.send_fail = true;

    });
  }

  function sendImgUpdate(data,pk){

    //var alpk = al;
    var imgpk = pk;

    var name_of_file = makeid();

    var img_data = blobToFile(data, name_of_file);

    var dataimg =  img_data;



    //alert('doing sendIMg'  + " " + imgpk + "  " + dataimg  );

    CreatePostService.img_up(file=dataimg,pk=imgpk).then(function(res){

     // console.log("success file uploaded");
     // alert('send success ' + res.data.id + res.data);
      //$state.go($state.current, $stateParams, {reload: true, inherit: false});
      $state.reload();



    }).catch(function(err){
     // alert("send_fail");
     // console.log("error did not upload");
     // console.error(err);
      //alert("send_fail" + err.data);
      //$state.go($state.current, $stateParams, {reload: true, inherit: false});
      $scope.send_fail = true;

    });
  }


function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'image/jpeg' });
}

function blobToFile(theBlob, fileName){
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return "app_" + text+ ".jpg";
}


  function getImageCamera(gpk,gal){

    var pass_on_al = gal;
    var pass_on_pk = gpk;


    document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 400,
      targetHeight: 400,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {

      image_file = "data:image/jpeg;base64," + imageData;
      
      //alert('getImage success');

      var data_con = dataURItoBlob(image_file);
       sendImg(data=data_con,pk=pass_on_pk,al=pass_on_al);
      
    }, 

    function(err) {
     // alert ('get image error')
      // error
    });

  }, false);

  }

  function getImageCameraUp(img_pk){

    //var pass_on_al = gal;
    //var pass_on_pk = gpk;
    var pass_on_pk = img_pk;


    document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 400,
      targetHeight: 400,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      
      image_file = "data:image/jpeg;base64," + imageData;
      
     // alert('getImage success');

      var data_con = dataURItoBlob(image_file);
       sendImgUpdate(data=data_con,pk=pass_on_pk);
      
    }, 

    function(err) {
      //alert ('get image error')
      // error
    });

  }, false);

  }



  function getImageFile(gpk,gal){

    var pass_on_al = gal;
    var pass_on_pk = gpk;


    document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 400,
      targetHeight: 400,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {

      image_file = "data:image/jpeg;base64," + imageData;
      
     // alert('getImage success');

      var data_con = dataURItoBlob(image_file);
       sendImg(data=data_con,pk=pass_on_pk,al=pass_on_al);
      
    }, 

    function(err) {
     // alert ('get image error');
      // error
    });

  }, false);

  }

   function getImageFileUp(gpk){

    //var pass_on_al = gal;
    var pass_on_pk = gpk;


    document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 400,
      targetHeight: 400,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {

      image_file = "data:image/jpeg;base64," + imageData;
      
      //alert('getImage success');

      var data_con = dataURItoBlob(image_file);
       sendImgUpdate(data=data_con,pk=pass_on_pk);
      
    }, 

    function(err) {
      //alert ('get image error');
      // error
    });

  }, false);

  }



function do_action (pkid, alid){
         // Show the action sheet
  var pk_id_send = pkid;
  var al_id_send = alid;
   var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: 'Take photo' },
       { text: 'Choose from file'}
     ],
     //destructiveText: 'Delete',
     titleText: 'Choose or Snap',
     cancelText: 'Cancel',
     
     cancel: function() {
       //   console.log("clicked cancel");
        },
     buttonClicked: function(index) {
     // console.log("clicked " + index);
       //return true;
       if (index === 0){
       // console.log("running one");
        getImageCamera(gpk=pk_id_send,gal=al_id_send);

       }
       else{
       // console.log("running zero");
        getImageFile(gpk=pk_id_send,gal=al_id_send);

       }

     },

   });
}

function do_change (pkid){
         // Show the action sheet
  var pk_id_send = pkid;
  //var al_id_send = alid;
   var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: 'Take photo' },
       { text: 'Choose from file'}
     ],
     //destructiveText: 'Delete',
     titleText: 'Choose or Snap',
     cancelText: 'Cancel',
     
     cancel: function() {
       //   console.log("clicked cancel");
        },
     buttonClicked: function(index) {
     // console.log("clicked " + index);
       //return true;
       if (index === 0){
      //  console.log("running one");
        getImageCameraUp(gpk=pk_id_send);

       }
       else{
       // console.log("running zero");
        getImageFileUp(gpk=pk_id_send);

       }

     },

   });

   // For example's sake, hide the sheet after two seconds
   $timeout(function() {
     hideSheet();
   }, 5000);

}

function delete_img(pk){
  //alert("calling service " + pk)
  CreatePostService.del_img(pk).then(function(res){
    $state.reload();
  }).catch(function(err){
     $ionicLoading.hide();
  });

  
}
  $scope.action_one = function(){do_action(pkid=1, alid=$scope.al_list_id);};
  $scope.action_two = function(){do_action(pkid=2, alid=$scope.al_list_id);};
  $scope.action_three = function(){do_action(pkid=3, alid=$scope.al_list_id);};
  $scope.action_four =function() {do_action(pkid=4, alid=$scope.al_list_id);};
  $scope.action_five = function() {do_action(pkid=5, alid=$scope.al_list_id);};

  $scope.do_change_one = function(){do_change(pkid=$scope.img_1_pk);};
  $scope.do_change_two = function(){do_change(pkid=$scope.img_2_pk);};
  $scope.do_change_three = function(){do_change(pkid=$scope.img_3_pk);};
  $scope.do_change_four = function(){do_change(pkid=$scope.img_4_pk);};
  $scope.do_change_five = function(){do_change(pkid=$scope.img_5_pk);};

  $scope.delete_one = function(){delete_img(pk = $scope.img_1_pk);};
  $scope.delete_two = function(){delete_img( pk = $scope.img_2_pk);};
  $scope.delete_three = function(){delete_img(pk = $scope.img_3_pk);};
  $scope.delete_four = function(){delete_img(pk =$scope.img_4_pk);};
  $scope.delete_five = function(){delete_img(pk =$scope.img_5_pk);};

  $scope.done_btn=function(){
    $state.go('app.profile');
  };





    

})

.controller('ProfileCtrl' ,function($scope, $state,$ionicActionSheet, MyAdsOps){
  var user_details = JSON.parse(localStorage.getItem('user'));
  //console.log (user_details);
  $scope.user_info = user_details;
  var ad_list = {};

  $scope.send_create=function(){
    $state.go('app.create');
   };

   function delete_ad(id){

   }

  function do_action_profile(id){
    var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: 'Preview' },
       { text: 'Edit'},
       { text: 'Delete'}
     ],
     //destructiveText: 'Delete',
     titleText: 'Choose or Snap',
     cancelText: 'Cancel',
     
     cancel: function() {
     //     console.log("clicked cancel");
        },
     buttonClicked: function(index) {
   //   console.log("clicked " + index);
    //  console.log(id);
      switch(index){
        case 0:
          $state.go('app.adpage', {adId:id});
          break;
        case 1:
          $state.go('app.editad', {adId:id});
          break;
        case 2:
         delete_ad(id);
         break;
        default:
          $state.go('app.adpage', {adId:id});
          break;
      }


       }

     });


  }

   MyAdsOps.get().then(function(res){
    this.ad_list = res.data;
   // console.log(this.ad_list);
    $scope.user_ad_list = res.data.results;
    $scope.more_link = res.data.next;
   })
   .catch(function(err){
  //  console.log('err');
   });

   $scope.do_action = function(id){
   // console.log(id);
    do_action_profile(id);
   };

})


.controller('PictureCtrl', function($scope, $cordovaCamera) {

 $scope.test_img = function(){ document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      var image = document.getElementById('myImage');
      image.src = "data:image/jpeg;base64," + imageData;
    }, function(err) {
      // error
    });

  }, false);



};

$scope.get_file = function(){
    document.addEventListener("deviceready", function () {

    var options = {
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
    };

    $cordovaCamera.getPicture(options).then(function(imageURI) {
      var image = document.getElementById('myImage');
      image.src = imageURI;
    }, function(err) {
      // error
    });


    $cordovaCamera.cleanup().then(); // only for FILE_URI

  }, false);
};

})

.controller("SearchCtrl", function($scope, SearchOps, $state, $http){
  $scope.data = [];
  $scope.show_me =false;
  $scope.show_moi = true;


 var params = {
  title: "",
  min_price:"",
  max_price:"",
  ad__category:"",
 };

function check_do_load(){
  if ($scope.next_url !== null){
    $scope.do_load = true;
  }
  else {
    $scope.do_load = false;
  }
}

  $scope.loadMoreData = function(){
   // alert("doing load more");
    if ($scope.next_url!== null){
      $http.get($scope.next_url).then(function(res){
     //   alert("success of get");
      angular.forEach(res.data.results, function(child){
    //  console.log(child);
      $scope.data.push(child);
    });
        $scope.next_url = res.data.next;
        $scope.$broadcast('scroll.infiniteScrollComplete');
        check_do_load();

      }). catch(function(err){
    //    console.log(err);
       // alert("error of get");
      });
    }
    else { $scope.do_load = false;}
  };
  $scope.simple_search = function(content){
    params.title = content.title;

    //send_string = JSON.stringify(params)

    SearchOps.search(params).then(function(res){
    //  console.log(res);
      $scope.data =[];
      $scope.next_url = res.data.next;

      angular.forEach(res.data.results, function(child){
    //  console.log(child);
      $scope.data.push(child);
    });

      if (res.data.count === 0){
        $scope.check_empty = true;
      }

      check_do_load();

    }).catch(function(err){

    });


  };
  $scope.filter_search = function(content){
    if (content.maxprice !== 'undefined'){
      params.max_price = content.maxprice;
    }

    if (content.minprice !== 'undefined'){
      params.min_price = content.minprice;
    }
    if (content.category!== 'undefined' && content.category !== 0){
      params.ad__category = content.category;
    }
    SearchOps.search(params).then(function(res){
   //   console.log(res);
      $scope.next_url = res.data.next;
      $scope.data = [];

      angular.forEach(res.data.results, function(child){
    //  console.log(child);
      $scope.data.push(child);
    });

      if (res.data.count === 0){
        $scope.check_empty = true;
      }

      check_do_load();

    }).catch(function(err){

    });
  };

  $scope.show_filter =function(){
    $scope.show_me = true;
    $scope.show_moi = false;

  };

  $scope.hide_filter =function(){
    $scope.show_me = false;
    $scope.show_moi = true;

  };


})

.controller("EventListCtrl", function($scope, EventOps, $state, $http){
  $scope.events = [];
  $scope.movies = [];
  $scope.event_show =true;
  $scope.outline_event = false;
  $scope.outline_movie = true;
  
  $scope.show_events = function(){
    $scope.event_show = true;
    $scope.outline_event = false;
    $scope.outline_movie = true;

  };
  $scope.show_movies = function(){
    $scope.event_show = false;
    $scope.outline_movie = true;
    $scope.outline_event = false;
  };

  $scope.do_load = false;


  EventOps.list_events().then(function(data){
   console.log(data);
    angular.forEach(data.data.results, function(child){
     // console.log(child);
      $scope.events.push(child);
    });
    //$scope.data.push(data.data.results);
    $scope.next_url = data.data.next;

    if ($scope.next_url !== null){
      $scope.do_load = true;
    }
  
   var checkEmpty = function (obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};

   $scope.check = checkEmpty(data.data.results);

  }).catch(function(err){
  //console.log(err);
});

  EventOps.list_movies().then(function(data){
   // console.log(data);
    angular.forEach(data.data.results, function(child){
     // console.log(child);
      $scope.movies.push(child);
    });
    //$scope.data.push(data.data.results);
    $scope.next_url = data.data.next;

    if ($scope.next_url !== null){
      $scope.do_load = true;
    }
  
   var checkEmpty = function (obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};

   $scope.check = checkEmpty(data.data.results);

  }).catch(function(err){
  //console.log(err);
});

function check_do_load(){
  if ($scope.next_url !== null){
    $scope.do_load = true;
  }
  else {
    $scope.do_load = false;
  }
}

  $scope.loadMoreData = function(){
    //alert("doing load more");
    if ($scope.next_url!== null){
      $http.get($scope.next_url).then(function(res){
      //  alert("success of get");
      angular.forEach(res.data.results, function(child){
     // console.log(child);
      $scope.data.push(child);
    });
        $scope.next_url = res.data.next;
        $scope.$broadcast('scroll.infiniteScrollComplete');
        check_do_load();

      }). catch(function(err){
      //  console.log(err);
      //  alert("error of get");
      });
    }
    else { $scope.do_load = false;}
  };




})


.controller("EventDetailCtrl", function($scope, EventOps, $state, $http, $stateParams){

  var send_me_there = $stateParams.eventID;
  var checkAvailable = function(){

  };

  EventOps.detail($stateParams.eventID).then(function(data){

    $scope.event = data.data;


  })
  .catch(function(err){
    console.log(err);
  });

  $scope.send_to_reserve= function(){

    $state.go('app.reserve_event', {eventID:send_me_there});};

})

.controller("ReserveEventCtrl", function($scope, EventOps, $stateParams){
  var user_details = JSON.parse(localStorage.getItem('user'));
  $scope.user_id = JSON.stringify(user_details.id);
  $scope.show_place=true;
  //$scope.event_id = $stateParams.event_ID;
  

  var init_me = function(){
    EventOps.detail($stateParams.eventID).then(function(data){
      $scope.event_info = data.data;

    }).catch(function(err){
      console.log(err);
    });
};
  init_me();


  $scope.do_reserve=function(data){
    var args = {
      "user": $scope.user_id,
      "event_id": $scope.event_info.id,
      "tel_no": data.tel,
      "price": $scope.event_info.ev_price,
    };

    EventOps.reserve(args).then(function(data){
      $scope.show_place = false;
      console.log(data);

    }).catch(function(err){
      console.log(err);
    });
  };


  
})

.controller('MessagesCtrl',function(){

})


.controller('InboxCtrl',function(){

})

.controller('OutboxCtrl',function(){

})

.controller('TrashCtrl',function(){

});