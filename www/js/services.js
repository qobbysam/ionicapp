//var BaseUrl = "http://127.0.0.1:8000/api"
var BaseUrl = "http://kadiel.com/api";
angular.module('starter.services', ['satellizer', 'ngFileUpload'])
.factory('AdService',function($http,$q){

  var deferred = $q.defer();
    var getad= function(pkl){
      return $http.get(BaseUrl+"/adp/"+pkl+"/");
    };

    return{get:getad};
})

.factory( 'CategoryService', function($http,$q){
  var deferred = $q.defer();
  var get= function (){
    $http.get( BaseUrl+'/cat/').success(deferred.resolve).error(deferred.resolve);

    return deferred.promise;
    };

  return {get:get};

})

.factory('CategoryPageService',function($http){

  
  var getads = function(cat_name){
    return $http.get(BaseUrl+'/cat-home/' + '?category=' + cat_name);    
  };
  return {get:getads};
})
.factory('CheckAuthStatus', function($auth, $q){

   var deferred = $q.defer();

    var CheckAuthStatus = function(){
        ans = $auth.isAuthenticated();

        if (ans){
            deferred.resolve("allow authenticated");
        }
        else{
            deferred.reject("authentication is required");
        }
        return deferred.promise;
    };

    return {check:CheckAuthStatus} ;
})

.factory('MyAdsOps', function($http){

  var get_my_ads = function(){
    return $http.get(BaseUrl+ '/my-ads/');
  };
  var delete_my_ad = function(pk){
    ad_to_del = $http({
      method:"DELETE",
      url: BaseUrl+"/"+pk+"/"
    });
    return ad_to_del;
  };


  return {get:get_my_ads, del:delete_my_ad};
})

.factory('CreatePostService', function($http, Upload){

  //alert("called post service");
  
  var send = function(data){
    var post_operation = $http({
       //transformRequest: angular.identity,
      method:"POST",
      url: BaseUrl+"/create-post/",


      data:data
    });
    return post_operation;

  };
  var ad_info = function(pk){
    var get_operation = $http.get(BaseUrl + "/imgs-pk/" + pk +"/");
    return get_operation;
  };

  var send_image=function(file,pk,al){
    var post_operation = $http({
      method:"POST",
      url: BaseUrl+"/one-img/"+pk+"/"+al+"/",
      data:data,
        headers : {
    'Content-Type': "image/jpeg"
  },
    });
    return post_operation;
  };

  var testUpload = function(file,pk,al) {
    //alert('sending image');
        var do_upload= Upload.upload({
            url: BaseUrl+"/one-img/"+pk+"/"+al+"/",
            data: {image:file},
            method: "POST"
        });
        return do_upload;
       };

  var testUpdate = function(file,pk) {
    //alert('sending image');
        var do_upload= Upload.upload({
            url: BaseUrl+"/up-img/"+pk+"/",
            data: {image:file},
            method: "PUT"
        });
        return do_upload;
       };

  var up_img = function(data,pk){

    var update_operation =  $http({
      method:"PUT",
      url: BaseUrl+"/up-img/"+pk+"/",
      data:data,
      headers: {
                        'Content-Type': undefined
                    }
    });
    return update_operation;

  };
  var get_img = function(pk){

    return $http.get(BaseUrl+"/up-img/"+pk+"/");

  };

  var delete_img = function(pk){
    return $http.delete(BaseUrl+"/up-img/" + pk + "/");
  };
  return {send:send, 
    prep:ad_info, 
    img_send:testUpload, 
    img_get:get_img, 
    img_up:testUpdate,
    del_img:delete_img
  };
})

. factory('SearchOps' ,function($http){
  var send = function(data_params){
    //console.log("called search");
   // console.log(data_params);
    return $http.get(BaseUrl+"/big-search/", {params:{  "title": data_params.title,
  "min_price":data_params.min_price,
  "max_price":data_params.max_price,
  "ad__category":data_params.ad__category}});
  };

  return {search:send};
})


.factory ('EventOps', function($http){

  var list_events = function(){

    return $http.get(BaseUrl+"/event/");

  };

  var list_movies = function(){

    return $http.get(BaseUrl+"/event/");

  };

  var detail = function(pk){
    return $http.get(BaseUrl+"/event/" + pk + "/");
  };

  var reserve_event = function(args){
    return $http.post(BaseUrl+"/reserve-event/", data=args);
  };

  return {list_events:list_events, 
          list_movies:list_movies,
          detail:detail,
          reserve:reserve_event
        };

})

.factory ('MailOps', function($http){
  var get_inbox = function(){
    return $http.get(BaseUrl+"/inbox-me/");
  };

  var get_outbox = function(){
    return $http.get(BaseUrl+"/inbox-me/");
  };

  var get_trash = function(){
    return $http.get(BaseUrl+"/inbox-me/");
  };

  var get_single_thread = function(id){

  };

  return {
    getInbox: get_inbox,
    getOutbox: get_outbox,
    getTrash: get_trash
  };
});




