// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services', 'templates'])

// .run(function($ionicPlatform) {
//   $ionicPlatform.ready(function() {
//     // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
//     // for form inputs)
//     if (window.cordova && window.cordova.plugins.Keyboard) {
//       cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
//       cordova.plugins.Keyboard.disableScroll(true);

//     }
//     if (window.StatusBar) {
//       // org.apache.cordova.statusbar required
//       StatusBar.styleDefault();
//     }
//   });
// })

.config(function($stateProvider, $urlRouterProvider) {
  

  $stateProvider

    .state('app', {
    cache:false,
    url: '/app',
    abstract: true,
    templateUrl: 'menu.html',
    controller: 'AppCtrl',

  })

  .state('app.search', {
    //cache:false,
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'search.html',
        controller: 'SearchCtrl'
      }
    }
  })

    .state('app.home', {
    cache: false,
    url: '/homepage',
    views: {
      'menuContent': {
        templateUrl: 'home.html',
        controller: 'homeCtrl'
      }
    }
  })
    .state('app.catsearch', {
    cache: false,
    url: '/catsearch/:name',
    views: {
      'menuContent': {
        templateUrl: 'cat_home.html',
        controller: 'CategoryhomeCtrl'
      }
    }
  })
    .state('app.adpage', {
    cache:false,
    url: '/adpage/:adId',
    views: {
      'menuContent': {
        templateUrl: 'ad_post.html',
        controller: 'AdCtrl'
      }
    }
  })
  .state('app.testimg', {
    cache:false,
    url: '/testimg',
    views: {
      'menuContent': {
        templateUrl: 'testimg.html',
        controller: 'PictureCtrl'
      }
    }
  })
// Event operations
// 
// -----------------
// -----------------
    .state('app.eventList', {
    cache:false,
    url: '/event',
    views: {
      'menuContent': {
        templateUrl: 'event_list.html',
        controller: 'EventListCtrl'
      }
    }
  })

      .state('app.event_detail', {
    cache:false,
    url: '/event-detail/:eventID',
    views: {
      'menuContent': {
        templateUrl: 'event_detail.html',
        controller: 'EventDetailCtrl'
      }
    }
  })

    .state('app.reserve_event', {
    cache: false,
    url: '/reserve-event/:eventID',
    views: {
      'menuContent': {
        templateUrl: 'reserve.html',
        controller: 'ReserveEventCtrl'
      },
        },
      data:{
        requiredLogin: true,
        nextUrl: "app.eventList"
      }
     })

//Create AdPost 
//
//
//-------------------------------
//
//
  .state('app.create', {
    cache: false,
    url: '/create',
    views: {
      'menuContent': {
        templateUrl: 'create_post.html',
        controller: 'CreatePostCtrl'
      },
        },
      data:{
        requiredLogin: true,
        nextUrl: "app.login"
      }
     })

    .state('app.addimgs', {
      cache:false,
      url:'/addimgs/:adId',
      views:{
        'menuContent': {
          templateUrl:'add_images.html',
          controller: 'AdImgsCtrl'
        }
      },
      data:{
        requiredLogin: true
      }
    })

       .state('app.editad', {
        cache:false,
      url:'/editad/:adId',
      views:{
        'menuContent': {
          templateUrl:'edit_post.html',
          controller: 'EditAdCtrl'
        }
      },
      
      data:{
        requiredLogin: true
      }
    })


    .state('app.visit_site', {
      url:'/visit-site',
      views:{
        'menuContent': {
          templateUrl:'visit.html',
          controller: 'EditAdCtrl'
        }
      },
      data:{
        requiredLogin: true
      }
    })


    // SEttings 
    // 
    // ----------------------------------
       .state('app.settings', {
      cache:false,
      url:'/settings',
      views:{
        'menuContent': {
          templateUrl:'settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })

    .state('app.profile', {
      cache:false,
      url:'/profile',
      views:{
        'menuContent': {
          templateUrl:'profile.html',
          controller: 'ProfileCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.profile"
      }
    })

    // Authentication operations
    // 
    // 
    // -------------------------------------
    
    .state('app.login', {
    cache: false,
    url: '/login/:next',
    views: {
      'menuContent': {
        templateUrl: 'login.html',
        controller: 'LoginCtrl'
      },
          }
  })
    .state('app.logout', {
    cache: false,
    url: '/logout',
    views: {
      'menuContent': {
        templateUrl: 'logout.html',
        controller: 'LogoutCtrl'
      },
          }
  })


/// messaging opps
/// 
/// ---------------
    .state('app.message', {

      cache:false,
      url:'/messages',
      views:{
        'menuContent': {
          templateUrl:'messages.html',
          controller: 'MessagesCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }

    })

    .state('app.inbox',{

      cache:false,
      url:'/messages/inboxme',
      views:{
        'menuContent': {
          templateUrl:'inbox.html',
          controller: 'InboxCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }

    })

    .state('app.outbox',{

      cache:false,
      url:'/messages/outboxme',
      views:{
        'menuContent': {
          templateUrl:'outbox.html',
          controller: 'OutboxCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }

    })

    .state('app.trash',{

      cache:false,
      url:'/messages/trashme',
      views:{
        'menuContent': {
          templateUrl:'trash.html',
          controller: 'TrashCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }

    })

    .state('app.read',{

      cache:false,
      url:'/profile',
      views:{
        'menuContent': {
          templateUrl:'read_message.html',
          controller: 'ReadMessageCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }
    })

    .state('app.send', {

      cache:false,
      url:'/messages/send',
      views:{
        'menuContent': {
          templateUrl:'send.html',
          controller: 'SendMessageCtrl'
        }
      },
      data:{
        requiredLogin: true,
        nextUrl: "app.message"
      }

    })

    .state('app.reply',{
      cache:false,
      url:'/messages/reply',
      views:{
        'menuContent': {
          templateUrl:'reply.html',
          controller: 'ReplyMessageCtrl'
        }
      },
      data:{
        requiredLogin: true

      }
    })

    .state('app.delete',{
      cache:false,
      url:'/messages/delete',
      views:{
        'menuContent': {
          templateUrl:'delete.html',
          controller: 'DeleteMessageCtrl'
        }
      },
      data:{
        requiredLogin: true
      }
    })

    
  
  .state('app.signup', {
    cache: false,
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'signup.html',
        controller: 'SignupCtrl'
      },
          }
  });
      // resolve:{
      //       authenticated: ['', function(djangoAuth){
      //       return djangoAuth.authenticationStatus(true);
      //     }],
      // }
    

  // if none of the above states are matched, use this as the fallback
  

  $urlRouterProvider.otherwise('/app/homepage');
})

//Handle social authentication
//
//-----------------------------
//
//-----------------------------

  .config(function($authProvider) {
    var commonConfig = {
      popupOptions: {
        location: 'no',
        toolbar: 'no',
        width: window.screen.width,
        height: window.screen.height
      }
    };

    if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
      $authProvider.cordova = true;
      commonConfig.redirectUri = "http://localhost:8100/";

    }
   

  $authProvider.baseUrl = 'http://kadiel.com/rest-auth';
  $authProvider.loginUrl = '/login/';
  $authProvider.signupUrl = '/registration/';
  $authProvider.tokenName= "key";
  $authProvider.authToken = "Token ";
  //$authProvider.withCredentials = true;
    

    $authProvider.facebook(angular.extend({}, commonConfig, {
      //redirectUri: window.location.origin + "/",
      clientId: '1518309871796743',
      url: 'http://kadiel.com/rest-auth/facebook/',
      

    }));
//   $authProvider.facebook({
//     clientId: '1518309871796743',
//   name: 'facebook',
//   url: '/facebook/',
//   authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
//   redirectUri: window.location.origin + '/',
//   requiredUrlParams: ['display', 'scope'],
//   scope: ['email'],
//   scopeDelimiter: ',',
//   display: 'popup',
//   type: '2.0',
//   popupOptions: { width: 580, height: 400 }
// });

    $authProvider.twitter(angular.extend({}, commonConfig, {
      url: '/twitter/'
    }));

    $authProvider.google(angular.extend({}, commonConfig, {
      clientId: '634986949090-826suvs5gm486j7utiobndqh40d0tf1d.apps.googleusercontent.com',
      url: '/google/'
    }));
  })
.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(sms|tel|mailto|https?|file):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
])
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .run(function ($rootScope, $state, $auth ) {
  $rootScope.$on('$stateChangeStart',
    function (event, toState) {
      var requiredLogin = false;
      // check if this state need login
      if (toState.data && toState.data.requiredLogin)
        requiredLogin = true;
      
      // if yes and if this user is not logged in, redirect him to login page
      if (requiredLogin && !$auth.isAuthenticated()) {
        var next = toState.data.nextUrl;
        event.preventDefault();
        $state.go('app.login', {next:next});
      }
    });
})

//show the loading spinner
//
//
//------------------------------
//

.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show');
        return config;
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide');
        return response;
      }
    };
  });
})

.run(function($rootScope, $ionicLoading) {
  $rootScope.$on('loading:show', function() {
    
    $ionicLoading.show({template: '<p>chargement...</p><ion-spinner></ion-spinner>'});
  });

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide();
  });
});